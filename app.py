from flask import Flask, render_template, request, jsonify
from chempy import balance_stoichiometry

app = Flask(__name__)


@app.route('/')
def user_route1():  # put application's code here
    return render_template("index.html")


@app.route('/stoich')
def dostoich():
    body = str(request.headers["input"])
    res = body.split("->")
    reac, prod = balance_stoichiometry(res[0].split(), res[1].split())
    res_str = " ".join([f"{k}: {v} " for k, v in dict(reac).items()]) + "-> " + " ".join([f"{k}: {v} " for k, v in dict(prod).items()])

    return {"result": res_str.strip()}


if __name__ == '__main__':
    print("stuff")
    app.run(port=4000)
