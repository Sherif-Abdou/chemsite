import React from "react";

interface Props {
    setProgram: (_: string) => void
}

const names = ["Stoich"];

export default function SideBarComponent(props: Props) {
    const listing = names.map(name => (
             <a className={"panel-block"} onClick={(e) => props.setProgram(name)}>
                 {name}
             </a>
    ));
    return (
        <nav className={"panel column is-3"}>
            <p className={"panel-heading"}>Programs</p>
            {listing}
        </nav>
    )
}