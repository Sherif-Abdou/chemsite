import React, {useState} from "react";
import SideBarComponent from "./SideBarComponent";
import StoichComponent from "./StoichComponent";

const states: Record<string, () => JSX.Element> = {
    "Stoich": () => (<StoichComponent />)
}

export default function AppComponent(props: any) {
    const [program, setProgram] = useState("Stoich")

    return (
        <div className={"columns"}>
            <SideBarComponent setProgram={setProgram}/>
            <div className={"column is-9"}>
                {states[program]()}
            </div>
        </div>
    );
}