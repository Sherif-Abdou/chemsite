import React, {useState} from "react";

const BASE_URL = window.location.href.replace(window.location.pathname, "")

export default function StoichComponent(props: any) {
    let [input, changeInput] = useState("");
    let [res, setRes] = useState("");

    const doStuff = (e: any) => {
        const headers = {
            "input": input
        }
        fetch("/stoich", {headers} )
            .then(res => res.json())
            .then(re => setRes(re.result));
    }

    return (
        <React.Fragment>
            <input value={input} onChange={(e) => changeInput(e.target.value)} className={"input"} />

            <button onClick={doStuff} className={"button"}>Balance</button>

            {res !== "" &&
                <p>{res}</p>
            }
        </React.Fragment>
    );
}